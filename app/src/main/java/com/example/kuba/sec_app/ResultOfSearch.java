package com.example.kuba.sec_app;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.Spinner;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class ResultOfSearch extends AppCompatActivity {
    private ListView listView;

    public void moveTo(int buildingID) {
        DatabaseAccess databaseAccess = DatabaseAccess.getInstance(this);
        databaseAccess.open();
        List<String> building = databaseAccess.getBuilding(buildingID);
        databaseAccess.close();
        Intent i = new Intent(ResultOfSearch.this, ShowObject.class);
        Bundle b = new Bundle();
        i.putExtra("obrazek", building.get(5));
        i.putExtra("name", building.get(2));
        i.putExtra("line1", building.get(1));
        i.putExtra("line2", building.get(3));
        i.putExtra("line3", building.get(4));
        i.putExtra("endLat", building.get(6));
        i.putExtra("endLng", building.get(7));
        startActivity(i);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.object_list);
        final Spinner dynamicSpinner = (Spinner) findViewById(R.id.dynamic_spinner);
        ArrayList<String> quotes = getIntent().getStringArrayListExtra("whereOut");

        Intent intent = getIntent();
        final HashMap<Integer, String> hashMap = (HashMap<Integer, String>) intent.getSerializableExtra("whereOutHasg");

        ArrayAdapter<String> adapter = new ArrayAdapter<String>(this, android.R.layout.simple_list_item_1, quotes);

        dynamicSpinner.setAdapter(adapter);
        dynamicSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {

            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                String Text = dynamicSpinner.getSelectedItem().toString();
                for (int key : hashMap.keySet()) {
                    if (hashMap.get(key).equals(Text)) {
                        Log.v("DebugHashInt", String.valueOf(key));
                        moveTo(key);
                    }
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
                // TODO Auto-generated method stub
            }
        });

        findViewById(android.R.id.content).post(new Runnable() {
            @Override
            public void run() {
                dynamicSpinner.performClick();
            }
        });
    }
}

