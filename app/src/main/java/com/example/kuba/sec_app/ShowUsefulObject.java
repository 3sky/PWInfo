package com.example.kuba.sec_app;

import android.Manifest;
import android.content.Context;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.location.Location;
import android.location.LocationManager;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.support.v4.content.ContextCompat;
import android.util.Log;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.maps.model.PolylineOptions;

import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class ShowUsefulObject extends FragmentActivity implements OnMapReadyCallback {

    private GoogleMap mMap;

        public ArrayList getPlaceByNature(String natu){
        DatabaseAccess databaseAccess = DatabaseAccess.getInstance(this);
        databaseAccess.open();
        ArrayList<String> uplace = databaseAccess.getUsefulPlace(natu);
        databaseAccess.close();
        return uplace;

    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_maps);
        // Obtain the SupportMapFragment and get notified when the map is ready to be used.
        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;
        mMap.setMapType(GoogleMap.MAP_TYPE_TERRAIN);


        Bundle b = getIntent().getExtras();
        String natu = b.getString("nature");
        ArrayList Markers = getPlaceByNature(natu);

        //Location on start point
        LatLng exampleMapBegin = new LatLng(Double.valueOf(Markers.get(1).toString()), Double.valueOf(Markers.get(2).toString()));
        mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(exampleMapBegin, 16));

        for (int i = 0; i <= Markers.size()-1 ; i+=3) {
            LatLng cord = new LatLng(Double.valueOf(Markers.get(i+1).toString()), Double.valueOf(Markers.get(i+2).toString()));
            mMap.addMarker(new MarkerOptions()
                    .position(cord)
                    .title(String.valueOf(Markers.get(i)))
                    .icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_RED)));
        }

    }


}
