package com.example.kuba.sec_app;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.ArrayAdapter;
import android.widget.Spinner;

import java.util.List;

public class UsefulObjectSpiner extends Activity{

    public void placeNature(String nature) {
        Intent i = new Intent(UsefulObjectSpiner.this, ShowUsefulObject.class);
        i.putExtra("nature", nature);
        startActivity(i);
    }

    private boolean isSpinnerTouched = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.object_list);
        final Spinner Spinner = (Spinner) findViewById(R.id.dynamic_spinner);
        DatabaseAccess databaseAccess = DatabaseAccess.getInstance(this);
        databaseAccess.open();
        List<String> routes = databaseAccess.getUsflulPlacesName();
        //routes.add(0, getString(R.string.trackSpinner));
        routes.add(0, "");
        databaseAccess.close();

        ArrayAdapter<String> adapter1 = new ArrayAdapter<>(this,
                android.R.layout.select_dialog_item, routes);

        Spinner.setAdapter(adapter1);


        Spinner.setOnItemSelectedListener(new OnItemSelectedListener() {

            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                String typ = Spinner.getSelectedItem().toString();
                if (!"".equals(typ))
                {
                    placeNature(typ);
                }

            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
                // TODO Auto-generated method stub
            }
        });

        findViewById(android.R.id.content).post(new Runnable() {
            @Override
            public void run() {
                Spinner.performClick();
            }
        });
    }
}

