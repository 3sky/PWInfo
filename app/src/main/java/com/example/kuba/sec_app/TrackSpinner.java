package com.example.kuba.sec_app;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Spinner;
import android.widget.AdapterView.OnItemSelectedListener;

import java.util.ArrayList;
import java.util.List;

public class TrackSpinner extends Activity{

    class MyRadioButton {
        private List<String> routes = new ArrayList<>();

        public List<String> getRoutes() {
            return (routes);
        }

        public List<String> addRoutes(List<String> a) {
            routes.clear();
            routes.addAll(a);
            routes.add(0, getString(R.string.trackSpinner1));
            return routes;
        }

        public List<String> noRoutes() {
            routes.clear();
            routes.add(0, getString(R.string.trackSpinner2));
            return routes;
        }
    }

    public void trackInfo(String jpg, String describtion) {
        Intent i = new Intent(TrackSpinner.this, ShowTrack.class);
        i.putExtra("obrazek", jpg);
        i.putExtra("opis", describtion);
        startActivity(i);
    }

    public String[] getDataAboutRoute (String sdisr){
        DatabaseAccess databaseAccess = DatabaseAccess.getInstance(this);
        databaseAccess.open();
        ArrayList<String> route = databaseAccess.getAllDescriptionsFromRouteArray(sdisr);
        databaseAccess.close();
        String desr = route.get(0);
        String img = route.get(1);
        return new String[] {img, desr};
    }

//    private boolean isSpinnerTouched = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.object_list_for_track);
        DatabaseAccess databaseAccess = DatabaseAccess.getInstance(this);
        databaseAccess.open();
        final List<String> routes = databaseAccess.getAvailableRoute();
        databaseAccess.close();
        final RadioButton r1 = (RadioButton) findViewById(R.id.radioButton1);
        final RadioButton r2 = (RadioButton) findViewById(R.id.radioButton2);
        final RadioGroup rg = (RadioGroup) findViewById(R.id.radioGroup) ;
        final MyRadioButton r = new MyRadioButton();


        Log.e("4---->", String.valueOf(r.getRoutes()));

        final Spinner trackSpinner1 = (Spinner) findViewById(R.id.dynamic_spinner);
        ArrayAdapter<String> adapter1 = new ArrayAdapter<>(this, android.R.layout.select_dialog_item, r.getRoutes());
        trackSpinner1.setAdapter(adapter1);

//        trackSpinner1.setOnTouchListener(new View.OnTouchListener() {
//            @Override
//            public boolean onTouch(View v, MotionEvent event) {
//                v.performClick();
//                isSpinnerTouched = true;
//                return false;
//            }
//        });

        trackSpinner1.setOnItemSelectedListener(new OnItemSelectedListener() {

            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
//                if (isSpinnerTouched) return;
                String short_name = trackSpinner1.getSelectedItem().toString();
                Log.e("5---->", String.valueOf(short_name));
                getDataAboutRoute(short_name);
                String result[] = getDataAboutRoute(short_name);
                trackInfo(result[0], result[1]);
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
                // TODO Auto-generated method stub
            }
        });

        rg.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {

            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId) {
                if(r1.isChecked())
                {
                    r.addRoutes(routes);
                    //Log.e("---->", String.valueOf(r.getRoutes()));
                }
                else if (r2.isChecked())
                {
                    r.noRoutes();
                    //Log.e("---->", String.valueOf(r.getRoutes()));
                }
                else {
                    Log.e("---->", "OK, here I'm");
                }
            }
        });

    }
}

