package com.example.kuba.sec_app;

import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.SparseArray;


import java.util.ArrayList;

import java.util.HashMap;
import java.util.List;

public class DatabaseAccess {
    private SQLiteOpenHelper openHelper;
    private SQLiteDatabase database;
    private static DatabaseAccess instance;

    /**
     * Private constructor to aboid object creation from outside classes.
     *
     * @param context
     */
    private DatabaseAccess(Context context) {
        this.openHelper = new DatabaseOpenHelper(context);
    }

    /**
     * Return a singleton instance of DatabaseAccess.
     *
     * @param context the Context
     * @return the instance of DabaseAccess
     */
    public static DatabaseAccess getInstance(Context context) {
        if (instance == null) {
            instance = new DatabaseAccess(context);
        }
        return instance;
    }

    /**
     * Open the database connection.
     */
    public void open() {
        this.database = openHelper.getWritableDatabase();
    }

    /**
     * Close the database connection.
     */
    public void close() {
        if (database != null) {
            this.database.close();
        }
    }

    /**
     *
     * @return a List of quotes
     */
    public List<String> getQuotes() {
        List<String> list = new ArrayList<>();
        Cursor cursor = database.rawQuery("SELECT NAME FROM pw_places", null);
        cursor.moveToFirst();
        while (!cursor.isAfterLast()) {
            list.add(cursor.getString(0));
            cursor.moveToNext();
        }
        cursor.close();
        return list;
    }
    /**
     *
     * @return a List of all information
     */
    public List<String> getBuilding(int buildingID) {
        List<String> list = new ArrayList<>();

        Cursor cursor = database.rawQuery("SELECT pp.ID,\n" +
                "       pp.CITY,\n" +
                "       pp.NAME,\n" +
                "       pp.ADDRESS,\n" +
                "       pp.POST_CODE,\n" +
                "       im.image_name,\n" +
                "       pp.LAT,\n" +
                "       pp.LANG\n" +
                "  FROM pw_places pp join image_name im\n" +
                "  on pp.image_id = im.ID WHERE pp.id = ?", new String [] {String.valueOf(buildingID)});
        cursor.moveToFirst();
        while (!cursor.isAfterLast()) {
            list.add(cursor.getString(0));
            list.add(cursor.getString(1));
            list.add(cursor.getString(2));
            list.add(cursor.getString(3));
            list.add(cursor.getString(4));
            list.add(cursor.getString(5));
            list.add(cursor.getString(6));
            list.add(cursor.getString(7));
            cursor.moveToNext();
        }
        cursor.close();
        return list;
    }
    /**
     *
     * @return a List of all objects contain word
     */
    public List<String> getBuildingWhereName(String namePart) {
        List<String> list = new ArrayList<>();

        StringBuilder buffer = new StringBuilder().append("%").append(namePart).append("%");
        Cursor cursor = database.rawQuery("SELECT NAME FROM pw_places WHERE NAME like ?", new String [] {String.valueOf(buffer)});
        cursor.moveToFirst();
        while (!cursor.isAfterLast()) {
            list.add(cursor.getString(0));
            cursor.moveToNext();
        }
        cursor.close();
        return list;
    }
    /**
     *
     * @return a Hashmap with ID and name
     */
    public HashMap<Integer, String> getBuildingWhereNameHash(String namePart) {
        HashMap<Integer, String> hmap = new HashMap<Integer, String>();

        StringBuilder buffer = new StringBuilder().append("%").append(namePart).append("%");
        Cursor cursor = database.rawQuery("SELECT ID, NAME FROM pw_places WHERE NAME like ?", new String [] {String.valueOf(buffer)});
        cursor.moveToFirst();
        while (!cursor.isAfterLast()) {
            hmap.put((cursor.getInt(0)),(cursor.getString(1)));
            cursor.moveToNext();
        }
        cursor.close();
        return hmap;
    }
    /**
     * Route indoore section
     * @return a List of available route in DB
     */
    public List<String> getAvailableRoute() {
        List<String> list = new ArrayList<>();
        Cursor cursor = database.rawQuery("SELECT short_description FROM route", null);
        cursor.moveToFirst();
        while (!cursor.isAfterLast()) {
            list.add(cursor.getString(0));
            cursor.moveToNext();
        }
        cursor.close();
        return list;
    }
    /**
     * Route indoore section
     * @return a arraylist with route_description and image.
     */
    public ArrayList<String> getAllDescriptionsFromRouteArray(String namePart) {
        ArrayList<String> tab = new ArrayList<String>();

        StringBuilder buffer = new StringBuilder().append("%").append(namePart).append("%");
        Cursor cursor = database.rawQuery("SELECT route_description, route_image FROM route WHERE short_description like ?", new String [] {String.valueOf(buffer)});
        cursor.moveToFirst();
        while (!cursor.isAfterLast()) {
            tab.add(cursor.getString(0));
            tab.add(cursor.getString(1));
            cursor.moveToNext();
        }
        cursor.close();
        return tab;
    }
    /**
     * Useful place section
     * @return a list with name of places nature.
     */
    public List<String> getUsflulPlacesName() {
        List<String> list = new ArrayList<>();
        Cursor cursor = database.rawQuery("SELECT nature FROM useful_place group by nature", null);
        cursor.moveToFirst();
        while (!cursor.isAfterLast()) {
            list.add(cursor.getString(0));
            cursor.moveToNext();
        }
        cursor.close();
        return list;
    }
    /**
     *
     * Useful place section
     * @return a list useful places's name, lat, lang contain selected nature type .
     */
    public ArrayList<String> getUsefulPlace(String natu) {
        ArrayList<String> list = new ArrayList<>();

        Cursor cursor = database.rawQuery("SELECT name,\n" +
                "       lat,\n" +
                "       lang\n" +
                "  FROM useful_place WHERE nature = ?", new String [] {natu});
        cursor.moveToFirst();
        while (!cursor.isAfterLast()) {
            list.add(cursor.getString(0));// cursor.getString(1),cursor.getString(2));
            list.add(cursor.getString(1));
            list.add(cursor.getString(2));
            cursor.moveToNext();
        }
        cursor.close();
        return list;
    }
    /**
     * Main building section
     * @return a list with name of Main building.
     */
    public List<String> getMainBuildingName() {
        List<String> list = new ArrayList<>();
        Cursor cursor = database.rawQuery("select official_name from main_building order by official_name", null);
        cursor.moveToFirst();
        while (!cursor.isAfterLast()) {
            list.add(cursor.getString(0));
            cursor.moveToNext();
        }
        cursor.close();
        return list;
    }
    /**
     *
     * Main building section
     * @return a list useful places's name, lat, lang contain selected Name .
     */
    public ArrayList<String> getMainBuildingName(String name) {
        ArrayList<String> list = new ArrayList<>();

        Cursor cursor = database.rawQuery("SELECT a.official_name,\n" +
                "       b.lat,\n" +
                "       b.lang\n" +
                "  from main_building as a join  pw_places as b\n" +
                "        on a.place_id = b.ID\n" +
                "        where a.official_name = ?", new String [] {name});
        cursor.moveToFirst();
        while (!cursor.isAfterLast()) {
            list.add(cursor.getString(0));// cursor.getString(1),cursor.getString(2));
            list.add(cursor.getString(1));
            list.add(cursor.getString(2));
            cursor.moveToNext();
        }
        cursor.close();
        return list;
    }
}
