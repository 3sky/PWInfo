package com.example.kuba.sec_app;

import android.Manifest;
import android.content.Context;
import android.content.pm.PackageManager;
import android.location.Location;
import android.location.LocationManager;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.support.v4.content.ContextCompat;
import android.util.Log;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;

import java.util.ArrayList;

public class ShowMainBuilding extends FragmentActivity implements OnMapReadyCallback {

      GoogleMap mMap;

        public ArrayList getBuildinByName(String name){
        DatabaseAccess databaseAccess = DatabaseAccess.getInstance(this);
        databaseAccess.open();
        ArrayList<String> uplace = databaseAccess.getMainBuildingName(name);
        databaseAccess.close();
        return uplace;

    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_maps);
        // Obtain the SupportMapFragment and get notified when the map is ready to be used.
        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;
        mMap.setMapType(GoogleMap.MAP_TYPE_TERRAIN);



        Bundle b = getIntent().getExtras();
        String n = b.getString("name");
        ArrayList Markers = getBuildinByName(n);
        LatLng cord = new LatLng(
                Double.valueOf(Markers.get(1).toString()),
                Double.valueOf(Markers.get(2).toString()));
        mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(cord, 16));
        mMap.addMarker(new MarkerOptions()
                    .position(cord)
                    .title(String.valueOf(Markers.get(0)))
                    .icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_RED)));


    }


}
