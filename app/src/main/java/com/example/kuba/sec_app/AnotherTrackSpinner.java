package com.example.kuba.sec_app;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.ArrayAdapter;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Spinner;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;

public class AnotherTrackSpinner extends Activity{


    public void trackInfo(String jpg, String describtion) {
        Intent i = new Intent(AnotherTrackSpinner.this, ShowTrack.class);
        i.putExtra("obrazek", jpg);
        i.putExtra("opis", describtion);
        startActivity(i);
    }

    public String[] getDataAboutRoute (String sdisr){
        DatabaseAccess databaseAccess = DatabaseAccess.getInstance(this);
        databaseAccess.open();
        ArrayList<String> route = databaseAccess.getAllDescriptionsFromRouteArray(sdisr);
        databaseAccess.close();
        String desr = route.get(0);
        String img = route.get(1);
        return new String[] {img, desr};
    }

    public void checked (String short_name) {
        getDataAboutRoute(short_name);
        String result[] = getDataAboutRoute(short_name);
        trackInfo(result[0], result[1]);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.another_object_list);
        final RadioButton r1 = (RadioButton) findViewById(R.id.radioButton1);
        final RadioButton r11 = (RadioButton) findViewById(R.id.radioButton11);
        final RadioButton r111 = (RadioButton) findViewById(R.id.radioButton111);
        final RadioButton r1111 = (RadioButton) findViewById(R.id.radioButton1111);
        final RadioButton r2 = (RadioButton) findViewById(R.id.radioButton2);
        final RadioGroup rg1 = (RadioGroup) findViewById(R.id.radioGroup);
        final RadioGroup rg2 = (RadioGroup) findViewById(R.id.radioGroup2);


        rg1.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {

            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId) {
                if(r1.isChecked()) {checked("Wejście -> Dziekanat Elektryczny (p. 133)");}
                else if (r11.isChecked()) {checked("Wejście -> Dziekanat GIK (p. 127)");}
                else if (r111.isChecked()) {checked("Wejście -> GEOIDA (p. 135)");}
                else if (r1111.isChecked()) {checked("Wejście -> Samorząd (p. 165)");}
            }
        });

        rg2.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {

            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId) {
                if(r2.isChecked())
                {
                    Context context = getApplicationContext();
                    CharSequence text = "No Data";
                    int duration = Toast.LENGTH_SHORT;
                    Toast toast = Toast.makeText(context, text, duration);
                    toast.show();
                }
                else {
                    Log.e("---->", "OK, here I'm");
                }
            }
        });
    }
}

