package com.example.kuba.sec_app;


import android.app.Dialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.ColorDrawable;
import android.support.v4.graphics.drawable.RoundedBitmapDrawable;
import android.support.v4.graphics.drawable.RoundedBitmapDrawableFactory;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;



public class ShowObject  extends AppCompatActivity {

    public Button GoToButton;


    public void zoomphoto (final String imageName) {

        ImageView myImageView = (ImageView) findViewById(R.id.ShowObjectImage);

        myImageView.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                Dialog builder = new Dialog(ShowObject.this);
                builder.requestWindowFeature(Window.FEATURE_NO_TITLE);
                builder.getWindow().setBackgroundDrawable(
                        new ColorDrawable(android.graphics.Color.TRANSPARENT));
                builder.setOnDismissListener(new DialogInterface.OnDismissListener() {
                    @Override
                    public void onDismiss(DialogInterface dialogInterface) {
                        //nothing;
                    }
                });
                BitmapFactory.Options options = new BitmapFactory.Options();
                options.inSampleSize = 5;
                Bitmap bitmap = BitmapFactory.decodeResource(getResources(),
                        getResources().getIdentifier(imageName, "drawable", getPackageName()),
                        options);
                ImageView imageView = new ImageView(ShowObject.this);
                imageView.setImageBitmap(bitmap);
                builder.addContentView(imageView, new RelativeLayout.LayoutParams(
                        ViewGroup.LayoutParams.MATCH_PARENT,
                        ViewGroup.LayoutParams.MATCH_PARENT));
                builder.show();
            }
        });
    }



    public void showRoute(double Lat, double Lng, String PlaceName){

        Intent showRoute = new Intent(ShowObject.this, NewMapActivity.class);
        showRoute.putExtra("endLat", Lat);
        showRoute.putExtra("endLng", Lng);
        showRoute.putExtra("endMarker",PlaceName);
        startActivity(showRoute);
    }

    public void startProccesData (final double Lat, final double Lng, final String name) {
        GoToButton = (Button) findViewById(R.id.GoToButton);
        GoToButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                showRoute(Lat, Lng, name);
            }
        });
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
         setContentView(R.layout.object_info);
         ImageView myImageView = (ImageView) findViewById(R.id.ShowObjectImage);
         Bundle b = getIntent().getExtras();
         String variableValue = b.getString("obrazek");
         BitmapFactory.Options options = new BitmapFactory.Options();
         options.inSampleSize = 8;
         Bitmap bitmap = BitmapFactory.decodeResource(getResources(),
                 getResources().getIdentifier(variableValue, "drawable", getPackageName()),
                 options);
         RoundedBitmapDrawable roundedBitmapDrawable = RoundedBitmapDrawableFactory.create(getResources(), bitmap);
         roundedBitmapDrawable.setCircular(true);
         myImageView.setImageDrawable(roundedBitmapDrawable);
         String name=b.getString("name");
         String line1=b.getString("line1");
         String line2=b.getString("line2");
         String line3=b.getString("line3");
         String LAT = b.getString("endLat");
         String LANG = b.getString("endLng");
         TextView tv=(TextView)findViewById(R.id.ShowObjectTitle);
         TextView l1=(TextView)findViewById(R.id.ShowObjectLine1);
         TextView l2=(TextView)findViewById(R.id.ShowObjectLine2);
         TextView l3=(TextView)findViewById(R.id.ShowObjectLine3);
         tv.setText(name);
         l1.setText(line1);
         l2.setText(line2);
         l3.setText(line3);
         startProccesData(Double.parseDouble(LAT),Double.parseDouble(LANG),name);
         zoomphoto(variableValue);
    }

}



