package com.example.kuba.sec_app;



import android.app.Dialog;
import android.content.DialogInterface;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.ColorDrawable;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.ImageView;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;


public class ShowTrack extends AppCompatActivity {

    public void showimage (Integer level) {
        ImageView myImageView = (ImageView) findViewById(R.id.ShowObjectImage);
        Bundle b = getIntent().getExtras();
        String a = b.getString("obrazek");
        String variableValue = "";
        if (level == 0) { variableValue = "base";}
        else if ( level == 1 ) { variableValue = a;}

        BitmapFactory.Options options = new BitmapFactory.Options();
        options.inSampleSize = 5;
        Bitmap bitmap = BitmapFactory.decodeResource(getResources(),
                getResources().getIdentifier(variableValue, "drawable", getPackageName()),
                options);
        myImageView.setImageBitmap(bitmap);
        zoomtrack(variableValue);
    }

    public void zoomtrack (final String imageName) {

        ImageView myImageView = (ImageView) findViewById(R.id.ShowObjectImage);

        myImageView.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                Dialog builder = new Dialog(ShowTrack.this);
                builder.requestWindowFeature(Window.FEATURE_NO_TITLE);
                builder.getWindow().setBackgroundDrawable(
                        new ColorDrawable(android.graphics.Color.TRANSPARENT));
                builder.setOnDismissListener(new DialogInterface.OnDismissListener() {
                    @Override
                    public void onDismiss(DialogInterface dialogInterface) {
                        //nothing;
                    }
                });
                BitmapFactory.Options options = new BitmapFactory.Options();
                options.inSampleSize = 2;
                Bitmap bitmap = BitmapFactory.decodeResource(getResources(),
                        getResources().getIdentifier(imageName, "drawable", getPackageName()),
                        options);
                ImageView imageView = new ImageView(ShowTrack.this);
                imageView.setImageBitmap(bitmap);
                builder.addContentView(imageView, new RelativeLayout.LayoutParams(
                        ViewGroup.LayoutParams.MATCH_PARENT,
                        ViewGroup.LayoutParams.MATCH_PARENT));
                builder.show();
            }
        });
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.track_shower);
        final RadioButton r1 = (RadioButton) findViewById(R.id.radioButton3);
        final RadioButton r2 = (RadioButton) findViewById(R.id.radioButton4);
        final RadioGroup rg1 = (RadioGroup) findViewById(R.id.radioGroup3);

        rg1.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {

            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId) {
                if(r1.isChecked()) {
                    showimage(0);

                } else if (r2.isChecked()) {
                    showimage(1);
                }
            }
        });


        Bundle b = getIntent().getExtras();
        String name=b.getString("opis");
        TextView tv=(TextView)findViewById(R.id.ShowObjectTitle);
        tv.setText(name);
    }

}



