package com.example.kuba.sec_app;

import android.content.Intent;
import android.os.Bundle;
import android.app.Activity;
import android.util.Log;
import android.util.SparseArray;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class GetUserInput extends Activity {

    Button mButton;
    EditText mEdit;
    private List<String> whereOut;
    private HashMap<Integer, String> whereOutHash;

    public void getNames(String namePart){

        DatabaseAccess databaseAccess = DatabaseAccess.getInstance(this);
        databaseAccess.open();
        List<String> whereOut = databaseAccess.getBuildingWhereName(namePart);
        HashMap<Integer, String> whereOutHash = databaseAccess.getBuildingWhereNameHash(namePart);
        databaseAccess.close();
        whereOut.add(0, "");
        Intent i = new Intent(GetUserInput.this, ResultOfSearch.class);
        i.putStringArrayListExtra("whereOut", (ArrayList<String>) whereOut);
        i.putExtra("whereOutHasg", (Serializable) whereOutHash);
        startActivity(i);
    }


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.get_user_input);
        mButton = (Button)findViewById(R.id.button1);

        mButton.setOnClickListener(new View.OnClickListener() {
            public void onClick(View view) {
                mEdit   = (EditText)findViewById(R.id.editText1);
                getNames(mEdit.getText().toString());
            }
        });
    }

}



